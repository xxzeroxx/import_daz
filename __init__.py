# Copyright (c) 2016-2021, Thomas Larsson
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.


bl_info = {
    "name": "DAZ (.duf, .dsf) importer",
    "author": "Thomas Larsson",
    "version": (1,6,1),
    "blender": (2,93,0),
    "location": "UI > Daz Importer",
    "description": "Import native DAZ files (.duf, .dsf)",
    "warning": "",
    "wiki_url": "http://diffeomorphic.blogspot.se/p/daz-importer-version-16.html",
    "tracker_url": "https://bitbucket.org/Diffeomorphic/import_daz/issues?status=new&status=open",
    "category": "Import-Export"}


def importModules():
    import os
    import importlib
    global theModules

    try:
        theModules
    except NameError:
        theModules = []

    if theModules:
        print("\nReloading DAZ")
        for mod in theModules:
            importlib.reload(mod)
    else:
        print("\nLoading DAZ")
        modnames = ["buildnumber", "globvars", "settings", "utils", "error", "uilist",
                    "propgroups", "daz", "fileutils", "load_json", "driver", "asset", "channels", "formula",
                    "transform", "node", "figure", "bone", "geometry", "objfile",
                    "fix", "modifier", "animation", "load_morph", "morphing", "panel",
                    "material", "cycles", "cgroup", "pbr", "render", "camera", "light",
                    "guess", "convert", "files", "merge", "main", "finger",
                    "matedit", "tables", "proxy", "rigify", "hide", "store",
                    "mhx", "layers", "hair", "transfer", "dforce",
                    "udim", "hdmorphs", "facecap", "api",
                    "runtime.morph_armature"]
        anchor = os.path.basename(__file__[0:-12])
        theModules = []
        for modname in modnames:
            mod = importlib.import_module("." + modname, anchor)
            theModules.append(mod)

import bpy
importModules()
from .settings import GS
from .api import *

#----------------------------------------------------------
#   Register
#----------------------------------------------------------

regnames = ["propgroups", "daz", "uilist", "driver",
            "figure", "geometry", "objfile",
            "fix", "animation", "morphing", "panel",
            "material", "cgroup", "render",
            "guess", "convert", "main", "finger",
            "matedit", "proxy", "rigify", "merge", "hide", "store",
            "mhx", "layers", "hair", "transfer", "dforce",
            "hdmorphs", "facecap", "udim"]

def register():
    for mod in theModules:
        modname = mod.__name__.rsplit(".",1)[-1]
        if modname in regnames:
            mod.register()
    GS.loadDefaults()


def unregister():
    for mod in reversed(theModules):
        modname = mod.__name__.rsplit(".",1)[-1]
        if modname in regnames:
            mod.unregister()


if __name__ == "__main__":
    register()

print("DAZ loaded")
